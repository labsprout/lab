package main

import "github.com/cucumber/godog"

func thereIsNoConfigFile() error {
	return godog.ErrPending
}

func iRunACommandUsingLab() error {
	return godog.ErrPending
}

func iShouldBePromptedForAnAuthenticationToken() error {
	return godog.ErrPending
}

func myCommandShouldExecute() error {
	return godog.ErrPending
}

func iLoginForTheFirstTimePassingTheTokenFlagAndAValidToken() error {
	return godog.ErrPending
}

func myTokenShouldBePersistedInTheConfigurationFile() error {
	return godog.ErrPending
}

func iShouldBeAbleToPerformTheRequestedOperation() error {
	return godog.ErrPending
}

func iRunACommandUsingLabAndPassAnIncorrectToken() error {
	return godog.ErrPending
}

func labShouldWarnMeThatTheTokenWasInvalid() error {
	return godog.ErrPending
}

func iRunACommandAndPassABlankToken() error {
	return godog.ErrPending
}

func labPromptsForATokenAgain() error {
	return godog.ErrPending
}

func iFailToEnterAValidTokenTimes(arg1 int) error {
	return godog.ErrPending
}

func labExits() error {
	return godog.ErrPending
}

func thereIsAConfigFileWithValidToken() error {
	return godog.ErrPending
}

func labReturnsTheOutputSuccessfully() error {
	return godog.ErrPending
}
func aNewConfigurationFileIsInitialized() error {
	return godog.ErrPending
}

func thereIsAConfigFileWithAnInvalidToken() error {
	return godog.ErrPending
}

func labFailesToExecuteTheCommand() error {
	return godog.ErrPending
}

func feedsBackTheFactTheTokenWasInvalidToTheUser() error {
	return godog.ErrPending
}

func iRunTheStatusCommandUsingLab() error {
	return godog.ErrPending
}

func labReturnsTheStatusSuccessfully() error {
	return godog.ErrPending
}

func exitsWithExitCode(arg1 int) error {
	return godog.ErrPending
}

func labExitsWithStatusCode(arg1 int) error {
	return godog.ErrPending
}

func FeatureContext(s *godog.Suite) {
	s.Step(`^there is no config file$`, thereIsNoConfigFile)
	s.Step(`^I run a command using lab$`, iRunACommandUsingLab)
	s.Step(`^I should be prompted for an authentication token$`, iShouldBePromptedForAnAuthenticationToken)
	s.Step(`^my command should execute$`, myCommandShouldExecute)
	s.Step(`^I login for the first time passing the token flag and a valid token$`, iLoginForTheFirstTimePassingTheTokenFlagAndAValidToken)
	s.Step(`^my token should be persisted in the configuration file$`, myTokenShouldBePersistedInTheConfigurationFile)
	s.Step(`^I should be able to perform the requested operation$`, iShouldBeAbleToPerformTheRequestedOperation)
	s.Step(`^I run a command using lab and pass an incorrect token$`, iRunACommandUsingLabAndPassAnIncorrectToken)
	s.Step(`^Lab should warn me that the token was invalid$`, labShouldWarnMeThatTheTokenWasInvalid)
	s.Step(`^I run a command and pass a blank token$`, iRunACommandAndPassABlankToken)
	s.Step(`^Lab prompts for a token again$`, labPromptsForATokenAgain)
	s.Step(`^I fail to enter a valid token (\d+) times$`, iFailToEnterAValidTokenTimes)
	s.Step(`^Lab exits$`, labExits)
	s.Step(`^there is a config file with valid token$`, thereIsAConfigFileWithValidToken)
	s.Step(`^Lab returns the output successfully$`, labReturnsTheOutputSuccessfully)
	s.Step(`^a new configuration file is initialized$`, aNewConfigurationFileIsInitialized)
	s.Step(`^there is a config file with an invalid token$`, thereIsAConfigFileWithAnInvalidToken)
	s.Step(`^Lab failes to execute the command$`, labFailesToExecuteTheCommand)
	s.Step(`^feeds back the fact the token was invalid to the user$`, feedsBackTheFactTheTokenWasInvalidToTheUser)
	s.Step(`^I run the status command using lab$`, iRunTheStatusCommandUsingLab)
	s.Step(`^Lab returns the status successfully$`, labReturnsTheStatusSuccessfully)
	s.Step(`^exits with exit code (\d+)$`, exitsWithExitCode)
	s.Step(`^Lab exits with status code (\d+)$`, labExitsWithStatusCode)
}
