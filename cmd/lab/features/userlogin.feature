Feature: authenticate with gitlab
  In order to use lab
  As a user of gitlab
  I need to be able authenticate with the gitlab server via token

  Scenario: Login for the first time with no arguments
    Given there is no config file
    When I run a command using lab
    Then I should be prompted for an authentication token
    And a new configuration file is initialized
    And my command should execute

  Scenario: Login for the first time with token flag set
    Given there is no config file
    When I login for the first time passing the token flag and a valid token
    Then my token should be persisted in the configuration file
    And I should be able to perform the requested operation

  Scenario: Login for first time with incorrect token
    Given there is no config file
    When I run a command using lab and pass an incorrect token
    Then Lab should warn me that the token was invalid

  Scenario: Login for first time with no token
    Given there is no config file
    When I run a command and pass a blank token
    Then Lab prompts for a token again
    And a new configuration file is initialized

  Scenario: User fails to enter token
    Given there is no config file
    When I fail to enter a valid token 3 times
    Then Lab exits with status code 1

  Scenario: Config file exists with valid token
    Given there is a config file with valid token
    When I run the status command using lab
    Then Lab returns the status successfully
    And exits with exit code 0

  Scenario: Config file exists with invalid token
    Given there is a config file with an invalid token
    When I run a command using lab
    Then Lab failes to execute the command
    And feeds back the fact the token was invalid to the user
