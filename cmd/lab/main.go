package main

import (
	"flag"
	"fmt"
	"github.com/xanzy/go-gitlab"
	"log"
)

func main() {

	argsPtr := flag.String("token", "foo", "a string")

	flag.Parse()

	fmt.Println("Running main")
	git, err := gitlab.NewClient(*argsPtr)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	users, _, err := git.Users.ListUsers(&gitlab.ListUsersOptions{})
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	for i, s := range users {
		fmt.Println(i, s)
	}
}
