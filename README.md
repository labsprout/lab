# LAB

A command line client for Gitlab

To compile:

    make
    
To run:

    ./_out/lab-mac -token <gitlabapitoken>
