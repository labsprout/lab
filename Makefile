PROJECT_NAME := lab
PKG := "gitlab.com/pixlise/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)

.PHONY: all build clean test coverage coverhtml lint

all: build test coverage lint

lint: ## Lint the files
	echo "${PKG}"
	#golint -set_exit_status ${PKG_LIST}
	golint ${PKG_LIST}

test: ## Run unittests
	mkdir -p _out
	#@go test -short ${PKG_LIST}
	go test -v ./... 2>&1 | go-junit-report > ./_out/report.xml

#race: ## Run data race detector
#	@go test ./... -race -short ${PKG_LIST}

#msan: ## Run memory sanitizer
#	@go test ./... -msan -short ${PKG_LIST}

#coverage: ## Generate global code coverage report
#	mkdir -p _out
#	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
# TODO: export GOPRIVATE=gitlab.com/pixlise ???
	mkdir -p _out
	./tools/coverage.sh html;

build: build-linux build-mac

build-linux:
	mkdir -p _out
	GOOS=linux GOARCH=amd64 go build -ldflags "-X main.apiVersion=${BUILD_VERSION} -X main.gitHash=${CI_COMMIT_SHA}" -v -o ./_out/lab-linux ./cmd/lab

build-mac:
	GOOS=darwin GOARCH=amd64 go build -ldflags "-X main.apiVersion=${BUILD_VERSION} -X main.gitHash=${CI_COMMIT_SHA}" -v -o ./_out/lab-mac ./cmd/lab

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

